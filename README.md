# Super Accelerated Computer Crash Course

Learn command line, vim, and C.
All you need is a Mac device.

## Introduction to the Terminal

Start by opening Finder. In Finder, you can see all the stuff that's stored on your computer. You may have used it before. You can click around and navigate through all the different files and folders. It's very useful. But what if I told you that you could do the same thing without having to drag around a cursor and click on things on your screen?

If you've used a computer recently, you've probably used a Graphical User Interface. That's just a fancy way of saying your computer has a mouse/trackpad/touchscreen that you can use to interact with the stuff on your screen. It's pretty intuitive, right? And you can do so much stuff, why bother with any other way of interacting with your computer?

You're about to find out! Before there were the computers that we know and love today, there were [computer terminals](https://en.wikipedia.org/wiki/Computer_terminal). All they had was a keyboard, and it turns out you can do a lot with that being your sole means of interacting with your computer. This text is meant to introduce you to the utility of the Command Line Interface at a basic level.

The first step is to open Terminal on your computer. You can go to `Utilities > Terminal` if you see it, or you can just press `Cmd-Space` and begin typing "terminal" until it comes up and then press `Enter`.

You'll see a window with a line of text that won't make much sense to you immediately, and that's ok. This line of text is called a prompt. All you need to know for now is that this is a place you can enter commands to get your computer to do stuff.

Try hitting `Enter` a few times. You should see a new prompt displayed with every press of the button. Now type the word "clear" and hit `Enter`. You should now just see one prompt. Congratulations, you've given the computer your first command!

Switch back to Finder. In the menu bar at the top, click `Go > Home`. This will show your home directory. You might not be familiar with it, but some of the folders in there should look familiar to you if you've used Finder before.

Switch back to Terminal, type the command `ls` and hit `Enter`. You should see all the same stuff you saw in Finder.

Take a moment to appreciate the progress you've made. You performed the same task--showing the contents of your home directory--using both Graphical User Interface in Finder and using Command Line Interface in Terminal.

Now get ready for something crazy: type `ls -a` in Terminal and hit `Enter`. Woah! Now you're seeing a bunch of stuff that doesn't show up in Finder. These are all your hidden files/directories. You'll notice that they all start with a `.`. 

## Introduction to Vim

Now get ready for something even crazier. We're going to create a new hidden file from the Terminal. Type `vim .vimrc` in Terminal and hit `Enter`.
Now you're inside the Vim text editor, and you can make changes to the new file you just created called `.vimrc`. We're going to configure vim and see our changes take place right before our eyes
set number
:source %
set ruler
:source %
etc etc

## Introduction to C

This is basically going to be the hello world tutorial from *The C Programming Language* by Brian Kernighan and Dennis Ritchie
Walk through how to write hello world in C in vim
ls -la to show permissions, difference between code file and executable program
